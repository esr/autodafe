#! /bin/sh
## Test faikure case CHECK_REQUIRE
#
# require.sh
#
# shellcheck disable=SC1091
. ./common-setup.sh

builddir="/tmp/require$$"
trap 'rm -fr $builddir' EXIT HUP INT QUIT TERM

here=$(pwd)

rm -fr "$builddir"

mkdir "${builddir}"

# shellcheck disable=SC2086
tapcd "${builddir}"

echo "# CHECK_HAVE(stdio.h)" >Makefile
echo "# CHECK_HAVE(bamfozzle.h) REQUIRED" >>Makefile

! "${here}/../configure" 2>/dev/null
tapout "CHECK_REQUIRE test failed, status $?"

echo "ok - require test"

# end

