#options: --enable=foo --bindir=/usr/fuzzlebin

# CHECK_ENABLE(foo, "Example foo option, you should see a -D for this")

# CHECK_ENABLE(bar, "Example bar option, you should not see a -D for this")

# CHECK_HAVE(gardafui.h)

# CHECK_HAVE(stdio.h, fputs)

# CHECK_HAVE(stdio.h, gronk)

# CHECK_BUILTIN(__builtin_bswap16)

# CHECK_BUILTIN(__builtin_foo)

# CHECK_DECL(stdio.h, BUFSIZ)

# CHECK_DECL(stdio.h, WRONGWAY)

# CHECK_LIB(math.h, m)

# CHECK_LIB(frogger.h, f)

# CHECK_HAVE(struct termios)

# CHECK_MEMBERS(time.h, struct timespec.tv_sec)

# CHECK_MEMBERS(time.h, struct nosuch.noway)

# CHECK_SIZEOF(char)

# CHECK_PROGRAM(sh)

# CHECK_PROGRAM(impossible-i-say)

# CHECK_SCRIPT(scriptdemo)

# These are architecture-dependent
## CHECK_SIZEOF(stdint.h, int32_t)
## CHECK_SIZEOF(termios.h, struct termios)
## CHECK_WORDS_BIGENDIAN
